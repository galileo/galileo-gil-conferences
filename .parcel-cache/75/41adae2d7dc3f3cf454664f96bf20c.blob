@import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600&family=Roboto:wght@400;500;700&display=swap');

/*! modern-normalize v1.0.0 | MIT License | https://github.com/sindresorhus/modern-normalize */

/*
Document
========
*/

/**
Use a better box model (opinionated).
*/

*,
*::before,
*::after {
  box-sizing: border-box;
}

/**
Use a more readable tab size (opinionated).
*/

:root {
  -moz-tab-size: 4;
  -o-tab-size: 4;
  tab-size: 4;
}

/**
1. Correct the line height in all browsers.
2. Prevent adjustments of font size after orientation changes in iOS.
*/

html {
  line-height: 1.15; /* 1 */
  -webkit-text-size-adjust: 100%; /* 2 */
}

/*
Sections
========
*/

/**
Remove the margin in all browsers.
*/

body {
  margin: 0;
}

/**
Improve consistency of default fonts in all browsers. (https://github.com/sindresorhus/modern-normalize/issues/3)
*/

body {
  font-family:
		system-ui,
		-apple-system, /* Firefox supports this but not yet `system-ui` */
		'Segoe UI',
		Roboto,
		Helvetica,
		Arial,
		sans-serif,
		'Apple Color Emoji',
		'Segoe UI Emoji';
}

/*
Grouping content
================
*/

/**
1. Add the correct height in Firefox.
2. Correct the inheritance of border color in Firefox. (https://bugzilla.mozilla.org/show_bug.cgi?id=190655)
*/

hr {
  height: 0; /* 1 */
  color: inherit; /* 2 */
}

/*
Text-level semantics
====================
*/

/**
Add the correct text decoration in Chrome, Edge, and Safari.
*/

abbr[title] {
  -webkit-text-decoration: underline dotted;
  text-decoration: underline dotted;
}

/**
Add the correct font weight in Edge and Safari.
*/

b,
strong {
  font-weight: bolder;
}

/**
1. Improve consistency of default fonts in all browsers. (https://github.com/sindresorhus/modern-normalize/issues/3)
2. Correct the odd 'em' font sizing in all browsers.
*/

code,
kbd,
samp,
pre {
  font-family:
		ui-monospace,
		SFMono-Regular,
		Consolas,
		'Liberation Mono',
		Menlo,
		monospace; /* 1 */
  font-size: 1em; /* 2 */
}

/**
Add the correct font size in all browsers.
*/

small {
  font-size: 80%;
}

/**
Prevent 'sub' and 'sup' elements from affecting the line height in all browsers.
*/

sub,
sup {
  font-size: 75%;
  line-height: 0;
  position: relative;
  vertical-align: baseline;
}

sub {
  bottom: -0.25em;
}

sup {
  top: -0.5em;
}

/*
Tabular data
============
*/

/**
1. Remove text indentation from table contents in Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=999088, https://bugs.webkit.org/show_bug.cgi?id=201297)
2. Correct table border color inheritance in all Chrome and Safari. (https://bugs.chromium.org/p/chromium/issues/detail?id=935729, https://bugs.webkit.org/show_bug.cgi?id=195016)
*/

table {
  text-indent: 0; /* 1 */
  border-color: inherit; /* 2 */
}

/*
Forms
=====
*/

/**
1. Change the font styles in all browsers.
2. Remove the margin in Firefox and Safari.
*/

button,
input,
optgroup,
select,
textarea {
  font-family: inherit; /* 1 */
  font-size: 100%; /* 1 */
  line-height: 1.15; /* 1 */
  margin: 0; /* 2 */
}

/**
Remove the inheritance of text transform in Edge and Firefox.
1. Remove the inheritance of text transform in Firefox.
*/

button,
select { /* 1 */
  text-transform: none;
}

/**
Correct the inability to style clickable types in iOS and Safari.
*/

button,
[type='button'] {
  -webkit-appearance: button;
}

/**
Remove the inner border and padding in Firefox.
*/

/**
Restore the focus styles unset by the previous rule.
*/

/**
Remove the additional ':invalid' styles in Firefox.
See: https://github.com/mozilla/gecko-dev/blob/2f9eacd9d3d995c937b4251a5557d95d494c9be1/layout/style/res/forms.css#L728-L737
*/

/**
Remove the padding so developers are not caught out when they zero out 'fieldset' elements in all browsers.
*/

legend {
  padding: 0;
}

/**
Add the correct vertical alignment in Chrome and Firefox.
*/

progress {
  vertical-align: baseline;
}

/**
Correct the cursor style of increment and decrement buttons in Safari.
*/

/**
1. Correct the odd appearance in Chrome and Safari.
2. Correct the outline style in Safari.
*/

/**
Remove the inner padding in Chrome and Safari on macOS.
*/

/**
1. Correct the inability to style clickable types in iOS and Safari.
2. Change font properties to 'inherit' in Safari.
*/

/*
Interactive
===========
*/

/*
Add the correct display in Chrome and Safari.
*/

summary {
  display: list-item;
}

/**
 * Manually forked from SUIT CSS Base: https://github.com/suitcss/base
 * A thin layer on top of normalize.css that provides a starting point more
 * suitable for web applications.
 */

/**
 * Removes the default spacing and border for appropriate elements.
 */

blockquote,
dl,
dd,
h1,
h2,
h3,
h4,
h5,
h6,
hr,
figure,
p,
pre {
  margin: 0;
}

button {
  background-color: transparent;
  background-image: none;
}

/**
 * Work around a Firefox/IE bug where the transparent `button` background
 * results in a loss of the default `button` focus styles.
 */

button:focus {
  outline: 1px dotted;
  outline: 5px auto -webkit-focus-ring-color;
}

fieldset {
  margin: 0;
  padding: 0;
}

ol,
ul {
  list-style: none;
  margin: 0;
  padding: 0;
}

/**
 * Tailwind custom reset styles
 */

/**
 * 1. Use the user's configured `sans` font-family (with Tailwind's default
 *    sans-serif font stack as a fallback) as a sane default.
 * 2. Use Tailwind's default "normal" line-height so the user isn't forced
 *    to override it to ensure consistency even when using the default theme.
 */

html {
  font-family: roboto, sans-serif; /* 1 */
  line-height: 1.5; /* 2 */
}

/**
 * Inherit font-family and line-height from `html` so users can set them as
 * a class directly on the `html` element.
 */

body {
  font-family: inherit;
  line-height: inherit;
}

/**
 * 1. Prevent padding and border from affecting element width.
 *
 *    We used to set this in the html element and inherit from
 *    the parent element for everything else. This caused issues
 *    in shadow-dom-enhanced elements like <details> where the content
 *    is wrapped by a div with box-sizing set to `content-box`.
 *
 *    https://github.com/mozdevs/cssremedy/issues/4
 *
 *
 * 2. Allow adding a border to an element by just adding a border-width.
 *
 *    By default, the way the browser specifies that an element should have no
 *    border is by setting it's border-style to `none` in the user-agent
 *    stylesheet.
 *
 *    In order to easily add borders to elements by just setting the `border-width`
 *    property, we change the default border-style for all elements to `solid`, and
 *    use border-width to hide them instead. This way our `border` utilities only
 *    need to set the `border-width` property instead of the entire `border`
 *    shorthand, making our border utilities much more straightforward to compose.
 *
 *    https://github.com/tailwindcss/tailwindcss/pull/116
 */

*,
::before,
::after {
  box-sizing: border-box; /* 1 */
  border-width: 0; /* 2 */
  border-style: solid; /* 2 */
  border-color: #e5e7eb; /* 2 */
}

/*
 * Ensure horizontal rules are visible by default
 */

hr {
  border-top-width: 1px;
}

/**
 * Undo the `border-style: none` reset that Normalize applies to images so that
 * our `border-{width}` utilities have the expected effect.
 *
 * The Normalize reset is unnecessary for us since we default the border-width
 * to 0 on all elements.
 *
 * https://github.com/tailwindcss/tailwindcss/issues/362
 */

img {
  border-style: solid;
}

textarea {
  resize: vertical;
}

input::-moz-placeholder, textarea::-moz-placeholder {
  color: #9ca3af;
}

input:-ms-input-placeholder, textarea:-ms-input-placeholder {
  color: #9ca3af;
}

input::placeholder,
textarea::placeholder {
  color: #9ca3af;
}

button,
[role="button"] {
  cursor: pointer;
}

table {
  border-collapse: collapse;
}

h1,
h2,
h3,
h4,
h5,
h6 {
  font-size: inherit;
  font-weight: inherit;
}

/**
 * Reset links to optimize for opt-in styling instead of
 * opt-out.
 */

a {
  color: inherit;
  text-decoration: inherit;
}

/**
 * Reset form element properties that are easy to forget to
 * style explicitly so you don't inadvertently introduce
 * styles that deviate from your design system. These styles
 * supplement a partial reset that is already applied by
 * normalize.css.
 */

button,
input,
optgroup,
select,
textarea {
  padding: 0;
  line-height: inherit;
  color: inherit;
}

/**
 * Use the configured 'mono' font family for elements that
 * are expected to be rendered with a monospace font, falling
 * back to the system monospace stack if there is no configured
 * 'mono' font family.
 */

pre,
code,
kbd,
samp {
  font-family: ui-monospace, SFMono-Regular;
}

/**
 * Make replaced elements `display: block` by default as that's
 * the behavior you want almost all of the time. Inspired by
 * CSS Remedy, with `svg` added as well.
 *
 * https://github.com/mozdevs/cssremedy/issues/14
 */

img,
svg,
video,
canvas,
audio,
iframe,
embed,
object {
  display: block;
  vertical-align: middle;
}

/**
 * Constrain images and videos to the parent width and preserve
 * their instrinsic aspect ratio.
 *
 * https://github.com/mozdevs/cssremedy/issues/14
 */

img,
video {
  max-width: 100%;
  height: auto;
}

h1 {
  font-size: 2.25rem;
  line-height: 2.5rem;
}

@media (min-width: 768px){
  h1{
    font-size: 3rem;
    line-height: 1;
  }
}

h2 {
  font-size: 1.5rem;
  line-height: 2rem;
}

@media (min-width: 768px){
  h2{
    font-size: 1.875rem;
    line-height: 2.25rem;
  }
}

h3 {
  font-size: 1.25rem;
  line-height: 1.75rem;
}

@media (min-width: 768px){
  h3{
    font-size: 1.5rem;
    line-height: 2rem;
  }
}

main a {
  --tw-text-opacity: 1;
  color: rgba(51, 51, 153, var(--tw-text-opacity));
  text-decoration: none;
}

.container{
  width: 100%;
}

@media (min-width: 640px){
  .container{
    max-width: 640px;
  }
}

@media (min-width: 768px){
  .container{
    max-width: 768px;
  }
}

@media (min-width: 1024px){
  .container{
    max-width: 1024px;
  }
}

@media (min-width: 1280px){
  .container{
    max-width: 1280px;
  }
}

@media (min-width: 1536px){
  .container{
    max-width: 1536px;
  }
}

.nav-item {
  background-color: transparent;
}

.nav-item:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(242, 245, 254, var(--tw-bg-opacity));
}

.nav-item:focus{
  --tw-bg-opacity: 1;
  background-color: rgba(242, 245, 254, var(--tw-bg-opacity));
}

.nav-item{
  border-radius: 0.5rem;
  font-weight: 600;
  font-size: 0.875rem;
  line-height: 1.25rem;
  margin-top: 0.5rem;
}

.nav-item:focus{
  outline: 2px solid transparent;
  outline-offset: 2px;
}

.nav-item{
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  padding-left: 1rem;
  padding-right: 1rem;
}

.nav-item:focus{
  --tw-ring-offset-shadow: var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color);
  --tw-ring-shadow: var(--tw-ring-inset) 0 0 0 calc(3px + var(--tw-ring-offset-width)) var(--tw-ring-color);
  box-shadow: var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000);
}

.nav-item:hover{
  --tw-text-opacity: 1;
  color: rgba(51, 51, 153, var(--tw-text-opacity));
}

.nav-item:focus{
  --tw-text-opacity: 1;
  color: rgba(51, 51, 153, var(--tw-text-opacity));
}

@media (min-width: 768px){
  .nav-item{
    margin-top: 0px;
    margin-left: 1rem;
  }
}

.button {
  border-radius: 0.375rem;
  font-weight: 500;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  padding-left: 1rem;
  padding-right: 1rem;
  --tw-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.05);
  box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
  letter-spacing: 0.025em;
}

@media (min-width: 1024px){
  .button{
    padding-left: 2rem;
    padding-right: 2rem;
  }
}

.button--action {
  --tw-bg-opacity: 1;
  background-color: rgba(165, 180, 252, var(--tw-bg-opacity));
}

.button--action:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(199, 210, 254, var(--tw-bg-opacity));
}

.button--action{
  --tw-text-opacity: 1;
  color: rgba(55, 48, 163, var(--tw-text-opacity));
}

.button--success {
  --tw-bg-opacity: 1;
  background-color: rgba(110, 231, 183, var(--tw-bg-opacity));
}

.button--success:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(167, 243, 208, var(--tw-bg-opacity));
}

.button--success{
  --tw-text-opacity: 1;
  color: rgba(6, 95, 70, var(--tw-text-opacity));
}

.button--warning {
  --tw-bg-opacity: 1;
  background-color: rgba(220, 53, 69, var(--tw-bg-opacity));
}

.button--warning:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(254, 202, 202, var(--tw-bg-opacity));
}

.button--warning{
  --tw-text-opacity: 1;
  color: rgba(255, 255, 255, var(--tw-text-opacity));
}

.space-x-3 > :not([hidden]) ~ :not([hidden]){
  --tw-space-x-reverse: 0;
  margin-right: calc(0.75rem * var(--tw-space-x-reverse));
  margin-left: calc(0.75rem * calc(1 - var(--tw-space-x-reverse)));
}

.sr-only{
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0, 0, 0, 0);
  white-space: nowrap;
  border-width: 0;
}

.bg-transparent{
  background-color: transparent;
}

.bg-black{
  --tw-bg-opacity: 1;
  background-color: rgba(0, 0, 0, var(--tw-bg-opacity));
}

.bg-white{
  --tw-bg-opacity: 1;
  background-color: rgba(255, 255, 255, var(--tw-bg-opacity));
}

.bg-gray-100{
  --tw-bg-opacity: 1;
  background-color: rgba(243, 244, 246, var(--tw-bg-opacity));
}

.bg-gray-300{
  --tw-bg-opacity: 1;
  background-color: rgba(209, 213, 219, var(--tw-bg-opacity));
}

.bg-gray-400{
  --tw-bg-opacity: 1;
  background-color: rgba(156, 163, 175, var(--tw-bg-opacity));
}

.bg-gray-800{
  --tw-bg-opacity: 1;
  background-color: rgba(31, 41, 55, var(--tw-bg-opacity));
}

.bg-red-400{
  --tw-bg-opacity: 1;
  background-color: rgba(248, 113, 113, var(--tw-bg-opacity));
}

.bg-red-500{
  --tw-bg-opacity: 1;
  background-color: rgba(239, 68, 68, var(--tw-bg-opacity));
}

.bg-yellow-400{
  --tw-bg-opacity: 1;
  background-color: rgba(251, 191, 36, var(--tw-bg-opacity));
}

.bg-green-400{
  --tw-bg-opacity: 1;
  background-color: rgba(52, 211, 153, var(--tw-bg-opacity));
}

.bg-blue-400{
  --tw-bg-opacity: 1;
  background-color: rgba(96, 165, 250, var(--tw-bg-opacity));
}

.bg-blue-light{
  --tw-bg-opacity: 1;
  background-color: rgba(242, 245, 254, var(--tw-bg-opacity));
}

.bg-blue{
  --tw-bg-opacity: 1;
  background-color: rgba(51, 51, 153, var(--tw-bg-opacity));
}

.bg-blue-darkest{
  --tw-bg-opacity: 1;
  background-color: rgba(28, 28, 78, var(--tw-bg-opacity));
}

.bg-indigo-500{
  --tw-bg-opacity: 1;
  background-color: rgba(99, 102, 241, var(--tw-bg-opacity));
}

.bg-purple-400{
  --tw-bg-opacity: 1;
  background-color: rgba(167, 139, 250, var(--tw-bg-opacity));
}

.bg-pink-400{
  --tw-bg-opacity: 1;
  background-color: rgba(244, 114, 182, var(--tw-bg-opacity));
}

.hover\:bg-gray-100:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(243, 244, 246, var(--tw-bg-opacity));
}

.hover\:bg-gray-200:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(229, 231, 235, var(--tw-bg-opacity));
}

.hover\:bg-red-200:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(254, 202, 202, var(--tw-bg-opacity));
}

.hover\:bg-yellow-200:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(253, 230, 138, var(--tw-bg-opacity));
}

.hover\:bg-green-200:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(167, 243, 208, var(--tw-bg-opacity));
}

.hover\:bg-blue-200:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(191, 219, 254, var(--tw-bg-opacity));
}

.hover\:bg-blue-light:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(242, 245, 254, var(--tw-bg-opacity));
}

.hover\:bg-indigo-400:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(129, 140, 248, var(--tw-bg-opacity));
}

.hover\:bg-purple-200:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(221, 214, 254, var(--tw-bg-opacity));
}

.hover\:bg-pink-200:hover{
  --tw-bg-opacity: 1;
  background-color: rgba(251, 207, 232, var(--tw-bg-opacity));
}

.focus\:bg-blue-light:focus{
  --tw-bg-opacity: 1;
  background-color: rgba(242, 245, 254, var(--tw-bg-opacity));
}

.bg-gradient-to-r{
  background-image: linear-gradient(to right, var(--tw-gradient-stops));
}

.from-black{
  --tw-gradient-from: #000;
  --tw-gradient-stops: var(--tw-gradient-from), var(--tw-gradient-to, rgba(0, 0, 0, 0));
}

.bg-opacity-25{
  --tw-bg-opacity: 0.25;
}

.border-gray-200{
  --tw-border-opacity: 1;
  border-color: rgba(229, 231, 235, var(--tw-border-opacity));
}

.border-gray-300{
  --tw-border-opacity: 1;
  border-color: rgba(209, 213, 219, var(--tw-border-opacity));
}

.border-red{
  --tw-border-opacity: 1;
  border-color: rgba(220, 53, 69, var(--tw-border-opacity));
}

.border-blue-light{
  --tw-border-opacity: 1;
  border-color: rgba(242, 245, 254, var(--tw-border-opacity));
}

.rounded{
  border-radius: 0.25rem;
}

.rounded-md{
  border-radius: 0.375rem;
}

.rounded-lg{
  border-radius: 0.5rem;
}

.rounded-full{
  border-radius: 9999px;
}

.border{
  border-width: 1px;
}

.border-b-2{
  border-bottom-width: 2px;
}

.border-b{
  border-bottom-width: 1px;
}

.block{
  display: block;
}

.inline-block{
  display: inline-block;
}

.inline{
  display: inline;
}

.flex{
  display: flex;
}

.inline-flex{
  display: inline-flex;
}

.table{
  display: table;
}

.grid{
  display: grid;
}

.hidden{
  display: none;
}

.flex-row{
  flex-direction: row;
}

.flex-col{
  flex-direction: column;
}

.flex-col-reverse{
  flex-direction: column-reverse;
}

.flex-wrap{
  flex-wrap: wrap;
}

.place-self-center{
  place-self: center;
}

.items-center{
  align-items: center;
}

.content-center{
  align-content: center;
}

.content-around{
  align-content: space-around;
}

.justify-center{
  justify-content: center;
}

.justify-between{
  justify-content: space-between;
}

.flex-1{
  flex: 1 1 0%;
}

.flex-auto{
  flex: 1 1 auto;
}

.flex-grow{
  flex-grow: 1;
}

.flex-shrink-0{
  flex-shrink: 0;
}

.font-heading{
  font-family: montserrat, sans-serif;
}

.font-normal{
  font-weight: 400;
}

.font-medium{
  font-weight: 500;
}

.font-semibold{
  font-weight: 600;
}

.font-bold{
  font-weight: 700;
}

.font-extrabold{
  font-weight: 800;
}

.font-black{
  font-weight: 900;
}

.h-3{
  height: 0.75rem;
}

.h-4{
  height: 1rem;
}

.h-5{
  height: 1.25rem;
}

.h-6{
  height: 1.5rem;
}

.h-10{
  height: 2.5rem;
}

.h-16{
  height: 4rem;
}

.h-24{
  height: 6rem;
}

.h-32{
  height: 8rem;
}

.h-48{
  height: 12rem;
}

.h-64{
  height: 16rem;
}

.h-full{
  height: 100%;
}

.h-screen{
  height: 100vh;
}

.text-xs{
  font-size: 0.75rem;
  line-height: 1rem;
}

.text-sm{
  font-size: 0.875rem;
  line-height: 1.25rem;
}

.text-base{
  font-size: 1rem;
  line-height: 1.5rem;
}

.text-xl{
  font-size: 1.25rem;
  line-height: 1.75rem;
}

.text-2xl{
  font-size: 1.5rem;
  line-height: 2rem;
}

.text-4xl{
  font-size: 2.25rem;
  line-height: 2.5rem;
}

.leading-tight{
  line-height: 1.25;
}

.leading-normal{
  line-height: 1.5;
}

.mx-4{
  margin-left: 1rem;
  margin-right: 1rem;
}

.mx-auto{
  margin-left: auto;
  margin-right: auto;
}

.mt-0{
  margin-top: 0px;
}

.mt-1{
  margin-top: 0.25rem;
}

.mb-1{
  margin-bottom: 0.25rem;
}

.ml-1{
  margin-left: 0.25rem;
}

.mt-2{
  margin-top: 0.5rem;
}

.mr-2{
  margin-right: 0.5rem;
}

.mb-2{
  margin-bottom: 0.5rem;
}

.ml-2{
  margin-left: 0.5rem;
}

.mr-3{
  margin-right: 0.75rem;
}

.mt-4{
  margin-top: 1rem;
}

.mr-4{
  margin-right: 1rem;
}

.mb-4{
  margin-bottom: 1rem;
}

.mt-5{
  margin-top: 1.25rem;
}

.mb-5{
  margin-bottom: 1.25rem;
}

.mt-8{
  margin-top: 2rem;
}

.mb-8{
  margin-bottom: 2rem;
}

.-mr-2{
  margin-right: -0.5rem;
}

.-ml-2{
  margin-left: -0.5rem;
}

.max-h-0{
  max-height: 0px;
}

.max-w-xl{
  max-width: 36rem;
}

.max-w-screen-md{
  max-width: 768px;
}

.max-w-screen-lg{
  max-width: 1024px;
}

.max-w-screen-xl{
  max-width: 1280px;
}

.min-h-screen{
  min-height: 100vh;
}

.object-cover{
  -o-object-fit: cover;
  object-fit: cover;
}

.opacity-0{
  opacity: 0;
}

.opacity-100{
  opacity: 1;
}

.focus\:outline-none:focus{
  outline: 2px solid transparent;
  outline-offset: 2px;
}

.overflow-hidden{
  overflow: hidden;
}

.overflow-scroll{
  overflow: scroll;
}

.overflow-y-scroll{
  overflow-y: scroll;
}

.p-2{
  padding: 0.5rem;
}

.p-3{
  padding: 0.75rem;
}

.p-4{
  padding: 1rem;
}

.p-6{
  padding: 1.5rem;
}

.p-10{
  padding: 2.5rem;
}

.p-16{
  padding: 4rem;
}

.py-2{
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
}

.px-2{
  padding-left: 0.5rem;
  padding-right: 0.5rem;
}

.px-4{
  padding-left: 1rem;
  padding-right: 1rem;
}

.py-5{
  padding-top: 1.25rem;
  padding-bottom: 1.25rem;
}

.px-5{
  padding-left: 1.25rem;
  padding-right: 1.25rem;
}

.py-6{
  padding-top: 1.5rem;
  padding-bottom: 1.5rem;
}

.px-6{
  padding-left: 1.5rem;
  padding-right: 1.5rem;
}

.px-8{
  padding-left: 2rem;
  padding-right: 2rem;
}

.py-10{
  padding-top: 2.5rem;
  padding-bottom: 2.5rem;
}

.pr-3{
  padding-right: 0.75rem;
}

.pr-4{
  padding-right: 1rem;
}

.pb-4{
  padding-bottom: 1rem;
}

.pt-10{
  padding-top: 2.5rem;
}

.pb-10{
  padding-bottom: 2.5rem;
}

.fixed{
  position: fixed;
}

.absolute{
  position: absolute;
}

.relative{
  position: relative;
}

.inset-0{
  top: 0px;
  right: 0px;
  bottom: 0px;
  left: 0px;
}

.inset-y-0{
  top: 0px;
  bottom: 0px;
}

.top-0{
  top: 0px;
}

.right-0{
  right: 0px;
}

.bottom-0{
  bottom: 0px;
}

.bottom-2{
  bottom: 0.5rem;
}

.right-3{
  right: 0.75rem;
}

.left-3{
  left: 0.75rem;
}

.right-5{
  right: 1.25rem;
}

*{
  --tw-shadow: 0 0 #0000;
}

.shadow-sm{
  --tw-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.05);
  box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
}

.shadow{
  --tw-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);
  box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
}

.shadow-lg{
  --tw-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05);
  box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000), var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
}

*{
  --tw-ring-inset: var(--tw-empty,/*!*/ /*!*/);
  --tw-ring-offset-width: 0px;
  --tw-ring-offset-color: #fff;
  --tw-ring-color: rgba(59, 130, 246, 0.5);
  --tw-ring-offset-shadow: 0 0 #0000;
  --tw-ring-shadow: 0 0 #0000;
}

.fill-current{
  fill: currentColor;
}

.table-auto{
  table-layout: auto;
}

.text-left{
  text-align: left;
}

.text-center{
  text-align: center;
}

.text-black{
  --tw-text-opacity: 1;
  color: rgba(0, 0, 0, var(--tw-text-opacity));
}

.text-white{
  --tw-text-opacity: 1;
  color: rgba(255, 255, 255, var(--tw-text-opacity));
}

.text-gray-100{
  --tw-text-opacity: 1;
  color: rgba(243, 244, 246, var(--tw-text-opacity));
}

.text-gray-600{
  --tw-text-opacity: 1;
  color: rgba(75, 85, 99, var(--tw-text-opacity));
}

.text-gray-700{
  --tw-text-opacity: 1;
  color: rgba(55, 65, 81, var(--tw-text-opacity));
}

.text-gray-900{
  --tw-text-opacity: 1;
  color: rgba(17, 24, 39, var(--tw-text-opacity));
}

.text-red{
  --tw-text-opacity: 1;
  color: rgba(220, 53, 69, var(--tw-text-opacity));
}

.text-blue-light{
  --tw-text-opacity: 1;
  color: rgba(242, 245, 254, var(--tw-text-opacity));
}

.text-blue{
  --tw-text-opacity: 1;
  color: rgba(51, 51, 153, var(--tw-text-opacity));
}

.hover\:text-blue:hover{
  --tw-text-opacity: 1;
  color: rgba(51, 51, 153, var(--tw-text-opacity));
}

.focus\:text-blue:focus{
  --tw-text-opacity: 1;
  color: rgba(51, 51, 153, var(--tw-text-opacity));
}

.uppercase{
  text-transform: uppercase;
}

.underline{
  text-decoration: underline;
}

.tracking-tight{
  letter-spacing: -0.025em;
}

.tracking-wide{
  letter-spacing: 0.025em;
}

.tracking-widest{
  letter-spacing: 0.1em;
}

.w-3{
  width: 0.75rem;
}

.w-4{
  width: 1rem;
}

.w-5{
  width: 1.25rem;
}

.w-6{
  width: 1.5rem;
}

.w-10{
  width: 2.5rem;
}

.w-16{
  width: 4rem;
}

.w-24{
  width: 6rem;
}

.w-48{
  width: 12rem;
}

.w-60{
  width: 15rem;
}

.w-full{
  width: 100%;
}

.z-10{
  z-index: 10;
}

.z-20{
  z-index: 20;
}

.z-40{
  z-index: 40;
}

.z-50{
  z-index: 50;
}

.gap-8{
  gap: 2rem;
}

.grid-cols-1{
  grid-template-columns: repeat(1, minmax(0, 1fr));
}

.transform{
  --tw-translate-x: 0;
  --tw-translate-y: 0;
  --tw-rotate: 0;
  --tw-skew-x: 0;
  --tw-skew-y: 0;
  --tw-scale-x: 1;
  --tw-scale-y: 1;
  transform: translateX(var(--tw-translate-x)) translateY(var(--tw-translate-y)) rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y)) scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));
}

.origin-top-right{
  transform-origin: top right;
}

.scale-95{
  --tw-scale-x: .95;
  --tw-scale-y: .95;
}

.scale-100{
  --tw-scale-x: 1;
  --tw-scale-y: 1;
}

.rotate-0{
  --tw-rotate: 0deg;
}

.rotate-180{
  --tw-rotate: 180deg;
}

.translate-x-0{
  --tw-translate-x: 0px;
}

.translate-x-full{
  --tw-translate-x: 100%;
}

.transition-all{
  transition-property: all;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 150ms;
}

.transition{
  transition-property: background-color, border-color, color, fill, stroke, opacity, box-shadow, transform;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 150ms;
}

.transition-transform{
  transition-property: transform;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 150ms;
}

.ease-in{
  transition-timing-function: cubic-bezier(0.4, 0, 1, 1);
}

.ease-out{
  transition-timing-function: cubic-bezier(0, 0, 0.2, 1);
}

.ease-in-out{
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
}

.duration-75{
  transition-duration: 75ms;
}

.duration-100{
  transition-duration: 100ms;
}

.duration-200{
  transition-duration: 200ms;
}

.duration-500{
  transition-duration: 500ms;
}

.duration-700{
  transition-duration: 700ms;
}

@-webkit-keyframes spin{
  to{
    transform: rotate(360deg);
  }
}

@keyframes spin{
  to{
    transform: rotate(360deg);
  }
}

@-webkit-keyframes ping{
  75%, 100%{
    transform: scale(2);
    opacity: 0;
  }
}

@keyframes ping{
  75%, 100%{
    transform: scale(2);
    opacity: 0;
  }
}

@-webkit-keyframes pulse{
  50%{
    opacity: .5;
  }
}

@keyframes pulse{
  50%{
    opacity: .5;
  }
}

@-webkit-keyframes bounce{
  0%, 100%{
    transform: translateY(-25%);
    -webkit-animation-timing-function: cubic-bezier(0.8,0,1,1);
    animation-timing-function: cubic-bezier(0.8,0,1,1);
  }

  50%{
    transform: none;
    -webkit-animation-timing-function: cubic-bezier(0,0,0.2,1);
    animation-timing-function: cubic-bezier(0,0,0.2,1);
  }
}

@keyframes bounce{
  0%, 100%{
    transform: translateY(-25%);
    -webkit-animation-timing-function: cubic-bezier(0.8,0,1,1);
    animation-timing-function: cubic-bezier(0.8,0,1,1);
  }

  50%{
    transform: none;
    -webkit-animation-timing-function: cubic-bezier(0,0,0.2,1);
    animation-timing-function: cubic-bezier(0,0,0.2,1);
  }
}

.text-shadow {
  text-shadow: 0 2px 4px rgba(0,0,0,0.20);
}

.swiper-pagination-bullet-active {
  background-color: #DC3545!important;
}

@media (min-width: 640px){
  .sm\:flex{
    display: flex;
  }

  .sm\:flex-col{
    flex-direction: column;
  }
}

@media (min-width: 768px){
  .md\:border-b-0{
    border-bottom-width: 0px;
  }

  .md\:border-r-2{
    border-right-width: 2px;
  }

  .md\:inline{
    display: inline;
  }

  .md\:flex{
    display: flex;
  }

  .md\:hidden{
    display: none;
  }

  .md\:flex-row{
    flex-direction: row;
  }

  .md\:flex-nowrap{
    flex-wrap: nowrap;
  }

  .md\:items-center{
    align-items: center;
  }

  .md\:justify-end{
    justify-content: flex-end;
  }

  .md\:justify-between{
    justify-content: space-between;
  }

  .md\:flex-1{
    flex: 1 1 0%;
  }

  .md\:flex-initial{
    flex: 0 1 auto;
  }

  .md\:h-36{
    height: 9rem;
  }

  .md\:text-lg{
    font-size: 1.125rem;
    line-height: 1.75rem;
  }

  .md\:text-2xl{
    font-size: 1.5rem;
    line-height: 2rem;
  }

  .md\:text-3xl{
    font-size: 1.875rem;
    line-height: 2.25rem;
  }

  .md\:text-4xl{
    font-size: 2.25rem;
    line-height: 2.5rem;
  }

  .md\:text-6xl{
    font-size: 3.75rem;
    line-height: 1;
  }

  .md\:mx-0{
    margin-left: 0px;
    margin-right: 0px;
  }

  .md\:mt-0{
    margin-top: 0px;
  }

  .md\:mb-0{
    margin-bottom: 0px;
  }

  .md\:mr-4{
    margin-right: 1rem;
  }

  .md\:ml-4{
    margin-left: 1rem;
  }

  .md\:-mt-1{
    margin-top: -0.25rem;
  }

  .md\:py-4{
    padding-top: 1rem;
    padding-bottom: 1rem;
  }

  .md\:px-4{
    padding-left: 1rem;
    padding-right: 1rem;
  }

  .md\:px-6{
    padding-left: 1.5rem;
    padding-right: 1.5rem;
  }

  .md\:py-20{
    padding-top: 5rem;
    padding-bottom: 5rem;
  }

  .md\:py-28{
    padding-top: 7rem;
    padding-bottom: 7rem;
  }

  .md\:px-28{
    padding-left: 7rem;
    padding-right: 7rem;
  }

  .md\:pb-0{
    padding-bottom: 0px;
  }

  .md\:pr-4{
    padding-right: 1rem;
  }

  .md\:pl-4{
    padding-left: 1rem;
  }

  .md\:pr-8{
    padding-right: 2rem;
  }

  .md\:pl-8{
    padding-left: 2rem;
  }

  .md\:pt-20{
    padding-top: 5rem;
  }

  .md\:pr-20{
    padding-right: 5rem;
  }

  .md\:pb-20{
    padding-bottom: 5rem;
  }

  .md\:right-10{
    right: 2.5rem;
  }

  .md\:left-10{
    left: 2.5rem;
  }

  .md\:text-left{
    text-align: left;
  }

  .md\:text-right{
    text-align: right;
  }

  .md\:w-36{
    width: 9rem;
  }

  .md\:w-48{
    width: 12rem;
  }

  .md\:w-auto{
    width: auto;
  }

  .md\:w-1\/2{
    width: 50%;
  }

  .md\:w-1\/3{
    width: 33.333333%;
  }

  .md\:w-1\/4{
    width: 25%;
  }

  .md\:w-5\/6{
    width: 83.333333%;
  }

  .md\:grid-cols-2{
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }

  .md\:h-500 {
    height: 500px;
  }
}

@media (min-width: 1024px){
  .lg\:flex-row{
    flex-direction: row;
  }

  .lg\:h-60{
    height: 15rem;
  }

  .lg\:text-5xl{
    font-size: 3rem;
    line-height: 1;
  }

  .lg\:-mr-4{
    margin-right: -1rem;
  }

  .lg\:-ml-4{
    margin-left: -1rem;
  }

  .lg\:px-8{
    padding-left: 2rem;
    padding-right: 2rem;
  }

  .lg\:px-16{
    padding-left: 4rem;
    padding-right: 4rem;
  }

  .lg\:w-60{
    width: 15rem;
  }

  .lg\:w-1\/4{
    width: 25%;
  }

  .lg\:w-4\/6{
    width: 66.666667%;
  }
}

@media (min-width: 1280px){
  .xl\:px-24{
    padding-left: 6rem;
    padding-right: 6rem;
  }

  .xl\:w-3\/6{
    width: 50%;
  }
}

@media (min-width: 1536px){
}