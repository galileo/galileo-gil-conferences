module.exports = {
  purge: [
    './*.html',
    './*.js',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        'sans': ['roboto', 'sans-serif'],
        'heading': ['montserrat', 'sans-serif'],
        'mono': ['ui-monospace', 'SFMono-Regular'],
        'display': ['Oswald'],
        'body': ['Open Sans'],
       },
      colors: {
        primary: '#333399',
        blue: {
          light: '#F2F5FE',
          DEFAULT: '#333399',
          dark: '#009eeb',
          darkest: '#1c1c4e',
        },
        red: {
          DEFAULT: '#DC3545',
        },
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
