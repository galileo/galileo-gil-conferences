# GALILEO and GIL Conferences

Both conferences share the same TailwindCSS CSS build. If adding NEW TailwindCSS utility classes to the LIVE EE templates, you will need to run another NPM Production build here and then upload the new CSS file to the SERVER.

To get started:

1. Install the dependencies:

    ```bash
    # Using npm
    npm install
    ```

2. Start the development server:

    ```bash
    # Using npm
    npm run start
    ```

## Building for production

To build an optimized version of your CSS, simply run:

```bash
# Using npm
npm run production
```
